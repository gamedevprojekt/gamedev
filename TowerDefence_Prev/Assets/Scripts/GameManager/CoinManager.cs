﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinManager : MonoBehaviour
{
    public int coins = 0;

    public Text coinCounterText;
	public Text coinCounterMenueText;
	public Text DmgTrap_lvlText;
	public Text DmgTrap_coastText;
	public Text SlowTrap_lvlText;
	public Text SlowTrap_coastText;
	public Text NormalTurret_lvlText;
	public Text NormalTurret_coastText;
	public Text HDTurret_lvlText;
	public Text HDTurret_coastText;
	public Text HRTurret_lvlText;
	public Text HRTurret_coastText;
	public Text ClickDmg_lvlText;
	public Text ClickDmg_coastText;
	public Text RebuildWalls_coastText;

	public Button DmgTrapUpgrade;
	public Button SlowTrapUpgrade;
	public Button NormalTurretUpgrade;
	public Button HDTurretUpgrade;
	public Button HRTurretUpgrade;
	public Button ClickDmgUpgrade;
	public Button RebuildWalls;

	private float upgradeLevelBasicTurret = 0.0f;
	private float upgradeLevelHighRangeTurret = 0.0f;
	private float upgradeLevelHighDamageTurret = 0.0f;
	private float upgradeLevelSlowTrap = 0.0f;
	private float upgradeLevelDamageTrap = 0.0f;
	private float upgradeLevelClickDamage = 0.0f;
	private float upgradeLevelRebuildWalls = 0.0f;
	private ToggleTree toggleTree;

	void Start()
	{

		toggleTree = GameController.FindObjectOfType (typeof(ToggleTree)) as ToggleTree;

		DmgTrap_lvlText.text = "Level: 0";
		DmgTrap_coastText.text = "Coast: " + calculateDamageTrapCoast().ToString();
		SlowTrap_lvlText.text = "Level: 0";
		SlowTrap_coastText.text = "Coast: " + calculateSlowTrapCoast().ToString();
		NormalTurret_lvlText.text = "Level: 0";
		NormalTurret_coastText.text = "Coast: " + calculateBasicTurretCoast ().ToString ();
		HDTurret_lvlText.text = "Level: 0";
		HDTurret_coastText.text = "Coast: " + calculateHighDamageTurretCoast().ToString();
		HRTurret_lvlText.text = "Level: 0";
		HRTurret_coastText.text = "Coast: " + calculateHighRangeTurretCoast().ToString();
		ClickDmg_lvlText.text = "Level: 0";
		ClickDmg_coastText.text = "Coast: " + calculateClickDamageCoast().ToString();
		RebuildWalls_coastText.text = "Coast: " + calculateRebuildWallsCoast ().ToString ();

		checkButton ();
	}

	/*
		Prüft ob die Button clickbar seinen sollen oder nicht.
	*/
	public void checkButton()
	{
		if(coins >= calculateDamageTrapCoast())
		{
			DmgTrapUpgrade.interactable = true;
		}
		else
		{
			DmgTrapUpgrade.interactable = false;
		}

		if(coins >= calculateSlowTrapCoast() && upgradeLevelSlowTrap < 2)
		{
			SlowTrapUpgrade.interactable = true;
		}
		else
		{
			SlowTrapUpgrade.interactable = false;
		}

		if(coins >= calculateBasicTurretCoast())
		{
			NormalTurretUpgrade.interactable = true;
		}
		else
		{
			NormalTurretUpgrade.interactable = false;
		}

		if(coins >= calculateHighDamageTurretCoast())
		{
			HDTurretUpgrade.interactable = true;
		}
		else
		{
			HDTurretUpgrade.interactable = false;
		}

		if(coins >= calculateHighRangeTurretCoast())
		{
			HRTurretUpgrade.interactable = true;
		}
		else
		{
			HRTurretUpgrade.interactable = false;
		}

		if(coins >= calculateClickDamageCoast())
		{
			ClickDmgUpgrade.interactable = true;
		}
		else
		{
			ClickDmgUpgrade.interactable = false;
		}

		if(coins >= calculateRebuildWallsCoast() && !toggleTree.checkWalls())
		{
			RebuildWalls.interactable = true;
		}
		else
		{
			RebuildWalls.interactable = false;
		}
	}

	public void upgrade(int ID)
	{
		switch(ID)
		{
		case 1: //Damage Trap
			removeCoins ((int)calculateDamageTrapCoast ());
			damageTrapUpgrade ();
			checkButton ();
			DmgTrap_lvlText.text = "Level: " + upgradeLevelDamageTrap.ToString ();
			DmgTrap_coastText.text = "Coast: " + calculateDamageTrapCoast().ToString();
			break;
		case 2: //Slow Trap
			removeCoins ((int)calculateSlowTrapCoast ());
			slowTrapUpgrade ();
			checkButton ();
			SlowTrap_lvlText.text = "Level: " + upgradeLevelSlowTrap.ToString ();
			SlowTrap_coastText.text = "Coast: " + calculateSlowTrapCoast().ToString();
			if(upgradeLevelSlowTrap == 2)
			{
				Text slowText = SlowTrapUpgrade.GetComponentInChildren<Text>();
				slowText.text = "MAX";
				SlowTrap_coastText.text = "Coast: -/-";
				SlowTrap_lvlText.text = "Level: MAX";
			}
			break;
		case 3: //Normal Turret
			removeCoins ((int)calculateBasicTurretCoast ());
			basicTurretUpgrade ();
			checkButton ();
			NormalTurret_lvlText.text = "Level: " + upgradeLevelBasicTurret.ToString ();
			NormalTurret_coastText.text = "Coast: " + calculateBasicTurretCoast().ToString();
			break;
		case 4: //HR Turret
			removeCoins ((int)calculateHighRangeTurretCoast ());
			highRangeTurretUpgrade ();
			checkButton ();
			HRTurret_lvlText.text = "Level: " + upgradeLevelHighRangeTurret.ToString ();
			HRTurret_coastText.text = "Coast: " + calculateHighRangeTurretCoast().ToString();
			break;
		case 5: //HD Turret
			removeCoins ((int)calculateHighDamageTurretCoast ());
			highDamageTurretUpgrade ();
			checkButton ();
			HDTurret_lvlText.text = "Level: " + upgradeLevelHighDamageTurret.ToString ();
			HDTurret_coastText.text = "Coast: " + calculateHighDamageTurretCoast().ToString();
			break;
		case 6: //Click Damage
			removeCoins ((int)calculateClickDamageCoast ());
			clickDmgUpgrade ();
			checkButton ();
			ClickDmg_lvlText.text = "Level: " + upgradeLevelClickDamage.ToString ();
			ClickDmg_coastText.text = "Coast: " + calculateClickDamageCoast().ToString();
			break;
		case 7: //Rebuild Walls
			removeCoins ((int)calculateRebuildWallsCoast ());
			rebuildWalls ();
			checkButton ();
			RebuildWalls_coastText.text = "Coast: " + calculateRebuildWallsCoast ().ToString ();
			break;

		default:
			Debug.Log ("falsche update ID!");
			break;
		}
	}

    public void addCoins(int amount)
    {
        if(amount > 0)
        {
            coins += amount;
            updateText();
        }
    }

    public void setCoins(int amount)
    {
        if(amount > 0)
        {
            coins = amount;
            updateText();
        }

    }

	public void removeCoins(int amount)
	{
		if(amount > 0)
		{
			coins -= amount;
			updateText ();
		}
	}

    public int getCoins()
    {
        return coins;
    }

    public void updateText()
    {
        coinCounterText.text = "Coins: " + coins;
		coinCounterMenueText.text = "Coins: \n" + coins;
    }

	public void basicTurretUpgrade(){
		this.upgradeLevelBasicTurret ++;

	}
	public void highRangeTurretUpgrade(){
		this.upgradeLevelHighRangeTurret++;

	}

	public void highDamageTurretUpgrade(){
		this.upgradeLevelHighDamageTurret++;

	}

	public void slowTrapUpgrade(){
		this.upgradeLevelSlowTrap++;

	}
	public void damageTrapUpgrade(){
		this.upgradeLevelDamageTrap++;

	}

	public void clickDmgUpgrade()
	{
		this.upgradeLevelClickDamage++;
	}

	public void rebuildWalls()
	{
		this.upgradeLevelRebuildWalls++;
	}


	public float calculateBasicTurretCoast()
	{
		return 270.0f + (Mathf.Pow (30.0f, this.upgradeLevelBasicTurret+1)/ Mathf.Pow (10.0f, this.upgradeLevelBasicTurret));
	}

	public float calculateSlowTrapCoast()
	{
		return 1800.0f + (Mathf.Pow (200.0f, this.upgradeLevelSlowTrap+1)/ Mathf.Pow (10.0f, this.upgradeLevelSlowTrap));
	}

	public float calculateHighRangeTurretCoast()
	{
		return 360.0f + (Mathf.Pow (40.0f, this.upgradeLevelHighRangeTurret+1)/ Mathf.Pow (10.0f, this.upgradeLevelHighRangeTurret));
	}

	public float calculateHighDamageTurretCoast()
	{
		return 360.0f + (Mathf.Pow (40.0f, this.upgradeLevelHighDamageTurret + 1) / Mathf.Pow (10.0f, this.upgradeLevelHighDamageTurret));
	}

	public float calculateDamageTrapCoast()
	{
		return 305.0f + (Mathf.Pow (45.0f, this.upgradeLevelDamageTrap+1)/ Mathf.Pow (15.0f, this.upgradeLevelDamageTrap));
	}

	public float calculateClickDamageCoast()
	{
		return 740.0f + (Mathf.Pow (60.0f, this.upgradeLevelClickDamage+1)/ Mathf.Pow (10.0f, this.upgradeLevelClickDamage));
	}
		
	public float calculateRebuildWallsCoast()
	{
		return 100 + (100 * this.upgradeLevelRebuildWalls);
	}
}


