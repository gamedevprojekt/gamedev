﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WaveManager : MonoBehaviour
{
    public float spawnTimeNormal = 0.02f;
    public Transform[] spawnPoints;
    public GameObject enemy;
    public GameObject runner;
	public GameObject boss;
	public GameController controller;
	public Text waveText;
    private int[] closeSpawnPoints = { 6, 7, 8, 16, 17, 18 };
	public int enemyCount = 10;
	private int lastWaveCount = 20;
	private int[] enemySelect = { 0, 0, 0, 0, 0,0,0,0,0, 1, 1, 1, 1 };
	private int  waveStatus ;
	private int waveCount = 1;
	private float clickDamage = 1.0f;
	private int basicEnemyLife = 1;
	private int runnerLife = 2;
	private int bosslife = 15;

   public void startWave()
    {
		
			InvokeRepeating("spawnWave", 2.0f, spawnTimeNormal);

    }

	public void nextWave()
	{
		int additionalEnemys = Random.Range(3, 12);
		enemyCount = lastWaveCount + additionalEnemys;
		lastWaveCount = enemyCount;

		waveStatus = 0;
	}

	void Update ()
	{
		if (waveStatus == 0) 
		{
			waveText.text = "Wave " + this.waveCount + " Started!";

		} 

		else if(waveStatus == 1)
		{
			waveText.text = "";
			if(checkEnemy())
			{
				waveStatus = 2;
				controller.end ();
				waveCount++;
				waveText.text = "Wave " + (this.waveCount -1) + " Ended!";
				calculateLife ();
			}
		}
	}

	private void spawnWave(){
		if(waveCount % 10 == 0 && enemyCount >0)
		{
			waveStatus = 1;
			enemyCount = 0;
			bosslife += 15;
			spawnBoss ();
		
		}else{
		if (enemyCount > 0) {
			
			waveStatus = 1;
			int selected = enemySelect [Random.Range (0, enemySelect.Length)];

			switch (selected) {
			case 0:
				spawnNormalEnemy ();
				break;

			case 1:
				spawnRunner ();
				break;
				

			}
			enemyCount--;
		}
	}
}

	private void calculateLife(){

		if(waveCount % 5 == 0){
			this.runnerLife++;
			this.basicEnemyLife++;
		}
	}

   private void spawnNormalEnemy()
    {
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        GameObject g = (GameObject)Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        BasicEnemy e = g.GetComponent<BasicEnemy>();
        setLife(g,basicEnemyLife);
        for (int i = 0; i < 6; i++)
        {
            if (spawnPointIndex == closeSpawnPoints[i])
            {
                e.setSpeed(1.5f);
            }
        }
    }

	private void spawnBoss()
	{
		int spawnPointIndex = Random.Range(0, spawnPoints.Length);
		GameObject g = (GameObject)Instantiate(boss, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
		Boss e = g.GetComponent<Boss>();
		setLife(g,bosslife);
		for (int i = 0; i < 6; i++)
		{
			if (spawnPointIndex == closeSpawnPoints[i])
			{
				e.setSpeed(0.05f);
			}
		}
	}

	private bool checkEnemy(){

		GameObject[] enemys = GameObject.FindGameObjectsWithTag ("Enemy");
		GameObject[] runners = GameObject.FindGameObjectsWithTag ("Runner");
		if (enemys.Length == 0 && runners.Length==0) {
			return true;
		} else {
			return false;
		}

	}

    private void spawnRunner()
    {
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        GameObject g = (GameObject)Instantiate(runner, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        RunnerEnemy e = g.GetComponent<RunnerEnemy>();
        setLife(g,runnerLife);
        for (int i = 0; i < 6; i++)
        {
            if (spawnPointIndex == closeSpawnPoints[i])
            {
                e.setSpeed(2.5f);
            }
        }

    }

	public int getWaveStatus(){
		return this.waveStatus;
	}

    private void setLife(GameObject e,int amount)
    {
        hitbox h = e.GetComponentInChildren<hitbox>();
        h.setLife(amount);
		h.setClickDamage (this.clickDamage);
    }

	public void setClickDamage(){
		this.clickDamage++;
	}
}
