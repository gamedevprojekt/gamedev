﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameRestart : MonoBehaviour 
{
	public void restart()
	{
		SceneManager.LoadScene ("TowerDefence");
	}

	public void exitGame()
	{
		Application.Quit ();
	}
}
