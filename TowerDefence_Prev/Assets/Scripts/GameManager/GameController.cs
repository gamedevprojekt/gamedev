﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    //public
    public Text looseText;
	public Text waveText;
    public CoinManager coinManager;
	public ToggleTree toggleTree;
	public Canvas menueGui;
	public Canvas gameGui;
	public Canvas buildMenueGui;
	public Canvas endRoundGui;
	public Canvas treeGui;
    private WaveManager waveManager;

    //start function
    void Start()
    {
        coinManager = GameController.FindObjectOfType(typeof(CoinManager)) as CoinManager;
        waveManager = GameController.FindObjectOfType(typeof(WaveManager)) as WaveManager;
		toggleTree = GameController.FindObjectOfType (typeof(ToggleTree)) as ToggleTree;
		toggleTree.placeWalls ();
        looseText.text = "";
		waveText.text = "";
        coinManager.setCoins(0);
		showGame ();
        waveManager.startWave();

    }



	public void end(){
		showEndRound ();
	}
	public void showEndRound()
	{
		buildMenueGui.enabled = false;
		menueGui.enabled = false;
		gameGui.enabled = true;
		endRoundGui.enabled = true;
	}

	public void showBuildMenue()
	{
		buildMenueGui.enabled = true;
		menueGui.enabled = false;
		gameGui.enabled = true;
		endRoundGui.enabled = false;
	}

	public void showMenue()
	{
		buildMenueGui.enabled = false;
		gameGui.enabled = true;
		menueGui.enabled = true;
		endRoundGui.enabled = false;
	}

	public void showGame()
	{
		menueGui.enabled = false;
		buildMenueGui.enabled = false;
		gameGui.enabled = true;
		endRoundGui.enabled = false;
		treeGui.enabled = false;
	}

    //if this object get destroyed
    void OnDestroy()
    {
        looseText.text = "You loose!";
		menueGui.enabled = true;
		endRoundGui.enabled = false;
		gameGui.enabled = true;
		buildMenueGui.enabled = false;
    }

}
