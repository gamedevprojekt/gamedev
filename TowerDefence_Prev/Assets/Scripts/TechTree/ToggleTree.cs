﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ToggleTree : MonoBehaviour 
{
	public Canvas techTree;
	public Canvas gameCanvas;
	public GameObject wallNorth;
	public GameObject wallEast;
	public GameObject wallSouth;
	public GameObject wallWest;

	public GameObject[] walls = new GameObject[4];


	void Start()
	{
		techTree.enabled = false;
	}

	public void Toggle()
	{
		if(techTree.enabled)
		{
			techTree.enabled = false;
			gameCanvas.enabled = true;
		}
		else
		{
			techTree.enabled = true;
			gameCanvas.enabled = false;
		}
	}

	public bool checkWalls()
	{
		bool wall = true;

		for(int i = 0; i < walls.Length; i++)
		{
			if(walls[i] == null)
			{
				wall = false;
			}
		}

		return wall;
	}

	public void placeWalls()
	{
		if (walls [0] == null) 
		{
			walls [0] = Instantiate (wallNorth);
		}

		if (walls [1] == null) 
		{
			walls [1] = Instantiate (wallEast);
		}

		if (walls [2] == null) 
		{
			walls [2] = Instantiate (wallSouth);
		}

		if (walls [3] == null) 
		{
			walls [3] = Instantiate (wallWest);
		}
	}





	public void upgradeBasicTurret()
	{
		GameObject basicTurretObject = GameObject.FindGameObjectWithTag ("BasicTurret");
		if (basicTurretObject != null) {
			BasicTurret basicTurret = basicTurretObject.GetComponent (typeof(BasicTurret)) as BasicTurret;
			basicTurret.upgradeDamage ();
		}
	}

	public void upgradeHighDpsTurret(){
		GameObject highDamgeTurretObject = GameObject.FindGameObjectWithTag ("DmgTurret");
		if (highDamgeTurretObject != null) {
			HighDPSTurret highDpsTurret = highDamgeTurretObject.GetComponent (typeof(HighDPSTurret)) as HighDPSTurret;
			highDpsTurret.upgradeDamage ();
		}
	}

	public void upgradeHighRangeTurret(){
		GameObject highRangeTurretObject = GameObject.FindGameObjectWithTag ("RangeTurret");
		if (highRangeTurretObject != null) {
			HighRangeTurret highRangeTurret = highRangeTurretObject.GetComponent (typeof(HighRangeTurret)) as HighRangeTurret;
			highRangeTurret.upgradeDamage ();
		}
	}

	public void upgradeDamageTrap(){
		GameObject dmgTrapObject = GameObject.FindGameObjectWithTag ("DmgTrap");
		if (dmgTrapObject != null) {
			DmgTrap dmgTrap = dmgTrapObject.GetComponent (typeof(DmgTrap)) as DmgTrap;
			dmgTrap.upgradeDamage ();
		}
	}

	public void upgradeSlowTrap(){
		GameObject slowTrapObject = GameObject.FindGameObjectWithTag ("SlowTrap");
		if (slowTrapObject != null) {
			SlowTrap slowTrap = slowTrapObject.GetComponent (typeof(SlowTrap)) as SlowTrap;
			slowTrap.upgradeSlow ();
		}
	}
}