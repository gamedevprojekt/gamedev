﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public interface EnemyInterface
{

    void Start();
    void FixedUpdate();
    void OnDestroy();
    void OnTriggerEnter(Collider other);
    void setSpeed(float speed);
    float getSpeed();
}
