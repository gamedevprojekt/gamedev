﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class BasicEnemy : MonoBehaviour, EnemyInterface
{
    //privat

    private Vector3 screenPoint;
    private Vector3 offset;
    private GameObject tower { get; set; }
    public hitbox hitbox;
    private float speed;
	private int  amount = 10;

 
    //Start function
    public void Start()
    {
        tower = GameObject.FindGameObjectWithTag("Tower");
     
    }

	public BasicEnemy(){
		this.speed = 2.5f;
	}

    //Update with physical aspect
    public void FixedUpdate()
    {
        var delta = tower.transform.position - transform.position;
        delta.Normalize();
        transform.position = transform.position + (delta * speed * Time.deltaTime);
    }

    public void OnDestroy()
    {
        tower.SendMessage("addCoins", amount);
    }

    //if the object get triggerd with a other collider
    public void OnTriggerEnter(Collider other)
	{
		//if the trigger is a Game Object with "Wall" tag
		if (other.gameObject.CompareTag ("Wall")) {
			amount = 0;
			Destroy (other.gameObject);
			if (!this.gameObject.CompareTag ("Boss")) {
			Destroy (this.gameObject);
		   }
		}
		//if the trigger is a Game Object with "Tower" tag
		if(other.gameObject.CompareTag("Tower"))
		{
			amount = 0;
			Destroy (other.gameObject);
			Destroy (this.gameObject);
		}
	}

    public void slowDown(float amount)
    {
        if(amount > 0)
        {
            this.speed -= amount;
        }
    }

    public void speedUp(float amount)
    {
        if(amount > 0)
        {
            this.speed += amount;
        }
    }

    public void setSpeed(float speed)
    {
        if(speed > 0)
        {
            this.speed = speed;
        }   
    }
    public float getSpeed()
    {
        return speed;
    }

    public void setTower(GameObject tower)
    {
        this.tower = tower;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

}
