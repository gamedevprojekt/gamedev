﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class hitbox :  MonoBehaviour
{

	public GameObject go;
	public Image lifeBar;
	private BasicEnemy g;
    private float life = 2;
	private float maxLife = 2;
	public float clickDamage = 1.0f;


    //Start function
    public void Start()
    {
		life = maxLife;
    }

	public void damage(float amount)
	{
		if (amount > 0.0f) 
		{
			if((life - amount) <= 0.0f)
			{
				destroy ();
			}
			else
			{
				life = life - amount;
			}
		}
	}

	void Update()
	{
		if(life <= 0)
		{
			destroy ();
		}

		lifeBar.fillAmount = life / maxLife;
	}

    public void setLife(float amount)
    {
		if(amount > 0)
		{
			maxLife = amount;
		}
    }
	public void setClickDamage(float damage){
		this.clickDamage = damage;

	}
    void OnMouseDown()
	{
		damage (clickDamage);
	}

    void destroy()
    {
        Destroy(go);
    }

    public float getLife()
    {
        return life;
    }

}
