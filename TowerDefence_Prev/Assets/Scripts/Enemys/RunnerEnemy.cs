﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class RunnerEnemy : BasicEnemy
{

    public RunnerEnemy()
    {
        setSpeed(4.0f);
        setAmount(15);
    }
}
