﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DamageNumbers : MonoBehaviour
	{
	    private float basicTurretDamage =1.0f;
	    private float highRangeTurretDamage =1.0f;
	    private float highDamageTurretDamage =2.0f;
	    private float damageTrapDamage =1.0f;
	    private float slowTrapSlowAmount = 1.0f;
	   
		public DamageNumbers ()
		{
		}
	  
	 public void upgradeBasicTurretDamage(){
		this.basicTurretDamage++;
	}
	public void upgradeSlowTrapSlowAmount(){
			this.slowTrapSlowAmount += 0.5f;
	}

	public void upgradeHighRangeTurretDamage(){
		this.highRangeTurretDamage++;
	  }
	public void upgradeHighDamageTurretDamage(){
		this.highDamageTurretDamage++;
	  }
	public void upgradeDamageTrapDamage(){
		this.damageTrapDamage++;
	  }
	 
	public float getBasicTurretDamage(){
		return this.basicTurretDamage;
	}
	public float getHighRangeTurretDamage(){
		return this.highRangeTurretDamage;
	}
	public float getHighDamageTurretDamage(){
		return this.highDamageTurretDamage;
	}
	public float getDamageTrapDamage(){
		return this.damageTrapDamage;
	}
	public float getSlowTrapSlowAmount(){
		return this.slowTrapSlowAmount;
	}


	}


