﻿using UnityEngine;
using System.Collections;

public interface TurretInterface
{
	void Start (); 
	void FixedUpdate ();
	void OnTriggerEnter(Collider other);
	void setRange(float amount);
	void setRate(float amount);
}
