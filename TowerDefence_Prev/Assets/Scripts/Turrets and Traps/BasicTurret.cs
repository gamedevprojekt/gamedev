﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasicTurret : MonoBehaviour, TurretInterface 
{
	public Transform gunEnd;
	protected float shootRate;
	protected float shootRange;
	private int coast =100;
	public float shootDamage;
	protected float nextFire;
	protected hitbox enemy;
	protected LineRenderer laserLine;
	public BoxCollider boxCollider;
	public Queue<hitbox> enemyQueue = new Queue<hitbox>();
	private DamageNumbers damage ;
	// Use this for initialization

	public void Start () 
	{
		setRange(7f);
		shootRate = 0.5f;
		damage = GameController.FindObjectOfType (typeof(DamageNumbers)) as DamageNumbers;
		shootDamage = damage.getBasicTurretDamage();
		laserLine = GetComponent<LineRenderer>();
	}
		

	// Update is called once per frame
	public void FixedUpdate () 
	{
		if(enemy != null)
		{
			this.gameObject.transform.LookAt (enemy.transform);
		}
		else
		{
			laserLine.enabled = false;
		}
	}

	protected void Update()
	{
		if (enemy != null && Time.time > nextFire) 
		{
			nextFire = Time.time + shootRate;

			laserLine.SetPosition (0, gunEnd.position);

			laserLine.SetPosition (1, enemy.transform.position);

			StartCoroutine (attack ());
		}

	}

	public void setCoast(int amount)
	{
		if(amount > 0)
		{
			coast = amount;
		}
	}

	public int getCoast()
	{
		return coast;
	}
		
	public void setRange(float amount)
	{
		if(amount > 0)
		{
			shootRange = amount;
			boxCollider.size = new Vector3 (shootRange, 0, shootRange);
		}
	}

	public void setRate(float amount)
	{
		if(amount > 0)
		{
			shootRate = amount;
		}
	}

	public void setDamage(float amount)
	{
		if (amount > 0) 
		{
			shootDamage = amount;
		}
	}

	public void upgradeDamage(){

		shootDamage++;
	}

	public void OnTriggerStay(Collider other)
	{
		if(enemy == null)
		{
			if (other.gameObject.GetComponentInParent<BasicEnemy> ()) 
			{
				enemy = other.gameObject.GetComponent<hitbox> ();
			}
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Hitbox")) 
		{
			if (enemy == null) 
			{
				if (other.gameObject.GetComponentInParent<BasicEnemy> ()) 
				{
					enemy = other.gameObject.GetComponent<hitbox> ();
					this.gameObject.transform.LookAt (enemy.transform);
				}
			}
		}
	}

	IEnumerator attack()
	{
		yield return new WaitForSeconds (0.02f);
		enemy.damage (shootDamage);
		laserLine.enabled = true;
		yield return new WaitForSeconds (0.08f);
		laserLine.enabled = false;
	}

	public void OnTriggerExit (Collider other)
	{
		if(enemy != null)
		{
			if(other.gameObject.GetComponent<hitbox>() == enemy)
			{
				enemy = null;
			}
		}
	}
		
}
