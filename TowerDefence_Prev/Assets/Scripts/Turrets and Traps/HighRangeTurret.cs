﻿using UnityEngine;
using System.Collections;


public class HighRangeTurret:  BasicTurret 
{
	private int coast = 110;
	new void Start()
	{
		setRange (14f);
		setDamage (0.5f);
		setRate (0.5f);

		laserLine = GetComponent<LineRenderer>();
	}

	new public int getCoast(){
		return coast;
	}
}