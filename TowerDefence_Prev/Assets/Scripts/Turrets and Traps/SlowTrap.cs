﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class SlowTrap : MonoBehaviour, TrapInterface
{
    private float slowAmount =1.0f;
	private int coast = 50;
	private DamageNumbers damageNumbers ;

    public SlowTrap()
    {
    }

    public void Start() { 
		damageNumbers = GameController.FindObjectOfType (typeof(DamageNumbers)) as DamageNumbers;
		slowAmount = damageNumbers.getSlowTrapSlowAmount();
	}

    public void OnTriggerEnter(Collider other)
    {

        if(other.gameObject.GetComponent("BasicEnemy") is BasicEnemy)
        {
            other.gameObject.GetComponentInParent<BasicEnemy>().slowDown(slowAmount);
        }

    }

    public void OnTriggerExit(Collider other)
    {
       
		if(other.gameObject.GetComponent("BasicEnemy") is BasicEnemy)
		{
			other.gameObject.GetComponentInParent<BasicEnemy>().speedUp(slowAmount);
		}
  
    }

	public void upgradeSlow(){
		this.slowAmount += 0.5f;
	}
    public void setSlow(float amount)
    {
       if(amount > 0)
        {
            slowAmount = amount;
        }
    }

	public int getCoast()
	{
		return coast;
	}
}


