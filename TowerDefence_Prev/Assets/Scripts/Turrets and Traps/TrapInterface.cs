﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public interface TrapInterface
{

	void Start();
	void OnTriggerEnter(Collider other);
}
