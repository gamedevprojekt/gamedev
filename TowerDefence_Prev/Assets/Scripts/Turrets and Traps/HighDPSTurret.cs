﻿using UnityEngine;
using System.Collections;


public class HighDPSTurret:  BasicTurret 
{
	private int coast = 110;
	new void Start()
	{
		setRange (4f);
		setDamage (2f);
		setRate (0.5f);

		laserLine = GetComponent<LineRenderer>();
	}

	new public int getCoast(){
		return coast;
	}
}