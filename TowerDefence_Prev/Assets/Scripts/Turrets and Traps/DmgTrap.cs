﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DmgTrap : MonoBehaviour, TrapInterface
{
    private float damage ;
	private int coast = 75;
	private DamageNumbers damageNumbers ;

	public DmgTrap ()
	{
	}

    public void Start() {
		damageNumbers = GameController.FindObjectOfType (typeof(DamageNumbers)) as DamageNumbers;
		damage = damageNumbers.getDamageTrapDamage();
	}
		
    public void OnTriggerEnter(Collider other)
    {
		
		if(other.gameObject.GetComponent("hitbox") is hitbox)
		{
			
            hitbox h = other.gameObject.GetComponent<hitbox>();
			h.damage(damage);
        }

    }

	public void upgradeDamage(){
		this.damage++;
	}
    public void setDamage(float damage)
    {
        this.damage = damage;
    }

	public int getCoast()
	{
		return coast;
	}
}


