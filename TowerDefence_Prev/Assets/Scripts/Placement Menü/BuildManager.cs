using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class BuildManager: MonoBehaviour
{
	public int SelectedBuilding;
	private int LastSelectedBuilding;
	public GameObject[] Building;
	

	private int[] supports = { 0, 0, 0, 0, 0 };
	private bool ghostOn = false;
	private GameObject ghost;
	private BuildingCollision ghostCollision;

	public bool isBuildingEnabled { get; private set; }


	public Text waveText;
	public CoinManager coinManager;
	private bool enoughMoney;

	void Start ()
	{
		LastSelectedBuilding = SelectedBuilding;
		isBuildingEnabled = false;

	}

	public void ActivateBuildingmode (int ID)
	{
		if (supports [ID] < 1) {
			isBuildingEnabled = true;
		}
        
	}

	public void DeactivateBuildingmode ()
	{
		isBuildingEnabled = false;
		if (ghostOn && ghost != null) {
			setBuilding ();
		}
	}
		
	/*
	 * Bestätigt das Gebäude auf der Map
	 */
	public void setBuilding ()
	{
		if (ghost != null && !ghostCollision.Collided ()) {
			
			waveText.text = "";
			if (ghost.CompareTag ("DmgTrap") || ghost.CompareTag ("SlowTrap")) {

				Instantiate (Building [SelectedBuilding],
					new Vector3 (ghost.transform.position.x,
						0.01f,
						ghost.transform.position.z),
					Quaternion.Euler (new Vector3 (90, 0, 0)));

			} else {
				Instantiate (Building [SelectedBuilding],
					new Vector3 (ghost.transform.position.x,
						0.01f,
						ghost.transform.position.z),
					Quaternion.identity);
			}

			DestroyImmediate (ghost);
			isBuildingEnabled = false;
			ghostOn = false;
			supports [SelectedBuilding] = 1;

		} else {
			waveText.text = "You can't build here";
		}
	}

	private int getCoast (GameObject ghost)
	{
		switch (ghost.tag) {
		case "BasicTurret":
			BasicTurret bt = ghost.GetComponent (typeof(BasicTurret)) as BasicTurret;
			if (coinManager.getCoins () >= bt.getCoast ()) {
				return bt.getCoast ();
			} else {
				return -1;	
			}

		case "DmgTurret":
			HighDPSTurret t = ghost.GetComponent (typeof(HighDPSTurret))as HighDPSTurret;

			if (coinManager.getCoins () >= t.getCoast ()) {
				return t.getCoast ();
			} else {
				return -1;	
			}

		case "RangeTurret":
			HighRangeTurret hrt = ghost.GetComponent (typeof(HighRangeTurret)) as HighRangeTurret;

			if (coinManager.getCoins () >= hrt.getCoast ()) {
				return hrt.getCoast ();
			} else {
				return -1;	
			}

		case "DmgTrap":
			DmgTrap dmgTrap = ghost.GetComponent (typeof(DmgTrap)) as DmgTrap;
			if (coinManager.getCoins () >= dmgTrap.getCoast ()) {
				return dmgTrap.getCoast ();
			} else {
				return -1;	
			}

		case "SlowTrap":
			SlowTrap slowTrap = ghost.GetComponent (typeof(SlowTrap)) as SlowTrap;
			if (coinManager.getCoins () >= slowTrap.getCoast ()) {
				return slowTrap.getCoast ();
			} else {
				return -1;	
			}
	

		default:
			return -1;
		}
	}

	/**
	 * Entfernt alle gesetzten Gebäude 
	 */
	public void deleteAllBuildings ()
	{
		int coins = 0;

		GameObject basicTurretObject = GameObject.FindGameObjectWithTag ("BasicTurret");
		GameObject highDamgeTurretObject = GameObject.FindGameObjectWithTag ("DmgTurret");
		GameObject highRangeTurretObject = GameObject.FindGameObjectWithTag ("RangeTurret");
		GameObject dmgTrapObject = GameObject.FindGameObjectWithTag ("DmgTrap");
		GameObject slowTrapObject = GameObject.FindGameObjectWithTag ("SlowTrap");

		if (highDamgeTurretObject != null) {	

			HighDPSTurret highDpsTurret = highDamgeTurretObject.GetComponent (typeof(HighDPSTurret))as HighDPSTurret;
			coins += highDpsTurret.getCoast ();
			Destroy (highDamgeTurretObject);
		}
		if (highRangeTurretObject != null) {
			HighRangeTurret highRangeTurret = highRangeTurretObject.GetComponent (typeof(HighRangeTurret)) as HighRangeTurret;
			coins += highRangeTurret.getCoast ();
			Destroy (highRangeTurretObject);
		}
		if (basicTurretObject != null) {
			BasicTurret basicTurret = basicTurretObject.GetComponent (typeof(BasicTurret)) as BasicTurret;
			coins += basicTurret.getCoast ();
			Destroy (basicTurretObject);
		}
		if (dmgTrapObject != null) {
			DmgTrap dmgTrap = dmgTrapObject.GetComponent (typeof(DmgTrap)) as DmgTrap;
			coins += dmgTrap.getCoast ();
			Destroy (dmgTrapObject);
		}
		if (slowTrapObject != null) {
			SlowTrap slowtrap = slowTrapObject.GetComponent (typeof(SlowTrap)) as SlowTrap;
			coins += slowtrap.getCoast ();
			Destroy (slowTrapObject);
		}
		ghost = null;
		ghostOn = false;
		coinManager.addCoins (coins);
		for (int i = 0; i < supports.Length; i++) {
			supports [i] = 0;
		}
	}

	/**
	 *Zum Auswählen des Gebäudes das zu stellen ist 
	 */
	public void SelectBuilding (int id)
	{
		if (supports [id] < 1) {
			if ((id < Building.Length) && (id >= 0) && (!ghostOn)) {
				if (getCoast (Building [id]) != -1) {
					LastSelectedBuilding = SelectedBuilding;
					SelectedBuilding = id;
					enoughMoney = true;
					coinManager.removeCoins (getCoast (Building [id]));
				} else {
					enoughMoney = false;
					waveText.text = "Not enough Coins!";
				}
			}

		} else {
			waveText.text = "You can't place more then one";
		}
	}


	public void Update ()
	{
		if (!isBuildingEnabled && ghost == null) {
			return;
		}

	            

		Ray ray;
		RaycastHit[] hit;

		if (Application.platform == RuntimePlatform.Android) {
			for (int j = 0; j < Input.touchCount; j++) {
				if (Input.GetTouch (j).phase == TouchPhase.Moved && (Input.GetTouch (j).deltaPosition.x > 0.0005f || Input.GetTouch (j).deltaPosition.y > 0.0005f)) {
				
					ray = Camera.main.ScreenPointToRay (Input.GetTouch (j).position);
					hit = Physics.RaycastAll (ray, Mathf.Infinity);
	         

		
					for (int i = 0; i < hit.Length; i++) {

						if (SelectedBuilding != LastSelectedBuilding && ghost != null) {
							Destroy (ghost);
							ghostOn = false;
							LastSelectedBuilding = SelectedBuilding;
						
							break;
						}
					
						if (!ghostOn && enoughMoney) {
						
							ghost = (GameObject)Instantiate (Building [SelectedBuilding], 
								new Vector3 (hit [i].point.x,
									hit [i].point.y, 
									hit [i].point.z), 
								Quaternion.identity);
						
							if (ghost.CompareTag ("DmgTrap") || ghost.CompareTag ("SlowTrap")) {

								ghost.transform.rotation = Quaternion.Euler (new Vector3 (90, 0, 0));

							}
							ghost.name = ghost.name.Replace ("(Clone)", "(Ghost)");
							ghost.layer = 2;

							GameObject cube = getChildGameObject (ghost.gameObject, "Cube");
							ghostCollision = cube.gameObject.AddComponent<BuildingCollision> ();
							ghostOn = true;	

						
						}
		
						if (ghost != null) {
							Vector3 ghostTargetPos = new Vector3 (
								                         hit [i].point.x,
								                         0.01f,
								                         hit [i].point.z);

							ghost.transform.position = ghostTargetPos;


							
							if (ghostCollision.Collided ()) {
								ghost.GetComponent<Renderer> ().material.CopyPropertiesFromMaterial (Building [SelectedBuilding].GetComponent<Renderer> ().sharedMaterial);
								ghost.GetComponent<Renderer> ().material.color = new Color (
									1f,
									0f, 
									0f, 
									0.6f);
							} else if (!ghostCollision.Collided ()) {
								ghost.GetComponent<Renderer> ().material.CopyPropertiesFromMaterial (Building [SelectedBuilding].GetComponent<Renderer> ().sharedMaterial);
								ghost.GetComponent<Renderer> ().material.color = new Color (
									0f,
									1f, 
									0f, 
									0.6f);
							}				
						}
					
					}
				}
			}
				
		} else if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor) {
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			hit = Physics.RaycastAll (ray, Mathf.Infinity);

			for (int i = 0; i < hit.Length; i++) {

				if (SelectedBuilding != LastSelectedBuilding && ghost != null) {
					Destroy (ghost);
					ghostOn = false;
					LastSelectedBuilding = SelectedBuilding;

					break;
				}

				if (!ghostOn && enoughMoney) {
					ghost = (GameObject)Instantiate (Building [SelectedBuilding], 
						new Vector3 (hit [i].point.x,
							0.01f, 
							hit [i].point.z), 
						Quaternion.identity);
					if (ghost.CompareTag ("DmgTrap") || ghost.CompareTag ("SlowTrap")) {
						ghost.transform.rotation = Quaternion.Euler (new Vector3 (90, 0, 0));
					}
					ghost.name = ghost.name.Replace ("(Clone)", "(Ghost)");
					ghost.layer = 2; //ignore raycast layer

					GameObject cube = getChildGameObject (ghost.gameObject, "Cube");
					ghostCollision = cube.gameObject.AddComponent<BuildingCollision> ();

					ghostOn = true;	
				}

				if (ghost != null) {
					if (Input.GetMouseButtonDown (0) && !ghostCollision.Collided ()) {
						


						if (ghost.CompareTag ("DmgTrap") || ghost.CompareTag ("SlowTrap")) {

							Instantiate (Building [SelectedBuilding],
								new Vector3 (ghost.transform.position.x,
									0.01f,
									ghost.transform.position.z),
								Quaternion.Euler (new Vector3 (90, 0, 0)));

						} else {
							Instantiate (Building [SelectedBuilding],
								new Vector3 (ghost.transform.position.x,
									0.01f,
									ghost.transform.position.z),
								Quaternion.identity);
						}

						DestroyImmediate (ghost);
							
						supports [SelectedBuilding] = 1;
					

						ghostOn = false;

						DeactivateBuildingmode ();

						break;
					} else if (ghostCollision.Collided ()) {
						waveText.text = "You can't build here";
					}

					Vector3 ghostTargetPos = new Vector3 (
						                         hit [i].point.x,
						                         0.01f,
						                         hit [i].point.z);

					ghost.transform.position = ghostTargetPos;



					if (ghostCollision.Collided ()) {

						ghost.GetComponent<Renderer> ().material.CopyPropertiesFromMaterial (Building [SelectedBuilding].GetComponent<Renderer> ().sharedMaterial);
						ghost.GetComponent<Renderer> ().material.color = new Color (
							1f,
							0f, 
							0f, 
							0.6f);
					} else if (!ghostCollision.Collided ()) {
						waveText.text = "";
						ghost.GetComponent<Renderer> ().material.CopyPropertiesFromMaterial (Building [SelectedBuilding].GetComponent<Renderer> ().sharedMaterial);
						ghost.GetComponent<Renderer> ().material.color = new Color (
							0f,
							1f, 
							0f, 
							0.6f);
					}

				}

			}
		}
	}


	public GameObject getChildGameObject (GameObject fromGameObject, string withName)
	{
		//Author: Isaac Dart, June-13.
		Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform> (true);
		foreach (Transform t in ts)
			if (t.gameObject.CompareTag ("spaceCollider"))
				return t.gameObject;
		return null;
	}

}