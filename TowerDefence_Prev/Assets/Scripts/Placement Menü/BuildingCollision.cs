using UnityEngine;
using System.Collections;


public class BuildingCollision : MonoBehaviour {
	
    private bool isCollided = false;
	public bool Collided()
    {
        return isCollided;
    }

 

    void Start()
    {
 
    }
	
	public void OnTriggerEnter(Collider other)
	{
		
		if (!other.gameObject.CompareTag ("BasicTurret") && !other.gameObject.CompareTag ("DmgTurret") && !other.gameObject.CompareTag ("RangeTurret") && !other.gameObject.CompareTag ("SlowTrap") && !other.gameObject.CompareTag ("DmgTrap")){
			isCollided = true;
		}
	}

	public void OnTriggerStay(Collider other)
	{
		if (!other.gameObject.CompareTag ("BasicTurret") && !other.gameObject.CompareTag ("DmgTurret") && !other.gameObject.CompareTag ("RangeTurret") && !other.gameObject.CompareTag ("SlowTrap") && !other.gameObject.CompareTag ("DmgTrap")){
			isCollided = true;
		}
	}

	void OnTriggerExit(Collider other)
	{

			isCollided = false;

	}
}
